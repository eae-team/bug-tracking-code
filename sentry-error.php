<?php
require __DIR__.'/vendor/autoload.php';

$sentryClient = new Raven_Client('SENTRY_PRIVATE_DSN');
$sentryClient->install();

try {
    throw new Exception('report exception');
} catch (Exception $e) {
    $sentryClient->captureException($e);
}



// Message at level 'info'
$sentryClient->captureMessage('info message', [], [
    'level' => Raven_Client::INFO
]);

// With extra data (3rd arg)
$sentryClient->captureMessage('info message', [], [
    'level' => Raven_Client::INFO,
    'extra' => [
        "some_key" => "some value"
    ]
]);

// raises an E_NOTICE which will be reported by the error handler
$foo = $bar;

// will be reported by the exception handler
throw new Exception('uncatch exception');

?>