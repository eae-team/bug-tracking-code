<?php
require __DIR__.'/vendor/autoload.php';

Rollbar::init(array('access_token' => 'ROLLBAR_SERVER_TOKEN'));

try {
    throw new Exception('report exception');
} catch (Exception $e) {
    Rollbar::report_exception($e);
}

// Message at level 'info'
Rollbar::report_message('info message', Level::INFO);

// With extra data (3rd arg) and custom payload options (4th arg)
Rollbar::report_message('info message', Level::INFO,
                        // key-value additional data
                        array("some_key" => "some value"),
                        // payload options (overrides defaults) - see api docs
                        array("fingerprint" => "custom-fingerprint-here"));

// raises an E_NOTICE which will *not* be reported by the error handler
$foo = $bar;

// will be reported by the exception handler
throw new Exception('uncatch exception');
?>